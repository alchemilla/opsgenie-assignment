package com.example.user.opsgenie_assignment.alert;

import com.birbit.android.jobqueue.JobManager;
import com.example.user.opsgenie_assignment.alert.job.AckAlertJob;
import com.example.user.opsgenie_assignment.alert.job.GetAlertDetailJob;
import com.example.user.opsgenie_assignment.alert.job.GetAlertJob;
import com.example.user.opsgenie_assignment.common.dagger.AppComponent;

import javax.inject.Inject;

/**
 * Created by User on 10/26/2017.
 */

public class AlertController {
    @Inject
    JobManager mJobManager;

    public AlertController(AppComponent appComponent) {
        appComponent.inject(this);
    }

    public void getAlerts(int offset, int limit) {
        mJobManager.addJobInBackground(new GetAlertJob(offset, limit));
    }

    public void getAlertDetail(String url, String apiKey) {
        mJobManager.addJobInBackground(new GetAlertDetailJob(url, apiKey));
    }

    public void ackAlert(String alertId) {
        mJobManager.addJobInBackground(new AckAlertJob(alertId));
    }
}
