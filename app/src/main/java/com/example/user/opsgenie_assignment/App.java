package com.example.user.opsgenie_assignment;

import android.app.Application;

import com.example.user.opsgenie_assignment.common.dagger.AppComponent;
import com.example.user.opsgenie_assignment.common.dagger.AppModule;
import com.example.user.opsgenie_assignment.common.dagger.DaggerAppComponent;
import com.google.firebase.messaging.FirebaseMessaging;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by User on 10/26/2017.
 */

public class App extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        EventBus.builder().addIndex(new EventBusIndex()).installDefaultEventBus();
        FirebaseMessaging.getInstance().subscribeToTopic("news");
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
