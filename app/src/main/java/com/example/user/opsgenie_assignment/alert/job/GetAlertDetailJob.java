package com.example.user.opsgenie_assignment.alert.job;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.example.user.opsgenie_assignment.alert.Alert;
import com.example.user.opsgenie_assignment.alert.event.AlertDetailLoadedEvent;
import com.example.user.opsgenie_assignment.common.ApiResponse;
import com.example.user.opsgenie_assignment.common.ApiService;
import com.example.user.opsgenie_assignment.common.base.BaseJob;
import com.example.user.opsgenie_assignment.common.dagger.AppComponent;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by User on 10/27/2017.
 */

public class GetAlertDetailJob extends BaseJob {

    @Inject
    ApiService mApiService;
    @Inject
    EventBus mEventBus;

    private String mUrl;
    private String mApiKey;

    public GetAlertDetailJob(String url, String apiKey) {
        super(new Params(PRIORITY_UX).requireNetwork().singleInstanceBy("get-alert-detail"));
        mApiKey = apiKey;
        mUrl = url;
    }

    @Override
    public void inject(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        Response<ApiResponse<Alert>> response = mApiService.getAlertDetail("alerts/" + mUrl, mApiKey, "id").execute();
        if (response.body() != null) {
            Alert alert = response.body().getData();
            mEventBus.post(new AlertDetailLoadedEvent(alert, true));
        } else {
            mEventBus.post(new AlertDetailLoadedEvent(null, false));
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        mEventBus.post(new AlertDetailLoadedEvent(null, false));
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return null;
    }
}
