package com.example.user.opsgenie_assignment.alert;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;

import com.example.user.opsgenie_assignment.R;
import com.example.user.opsgenie_assignment.alert.event.AlertEvent;
import com.example.user.opsgenie_assignment.alert.event.PageRequestEvent;
import com.example.user.opsgenie_assignment.common.base.BaseActivity;
import com.example.user.opsgenie_assignment.databinding.ActivityMainBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

public class AlertListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    public static final int LIMIT = 20;

    @Inject
    EventBus mEventBus;
    @Inject
    AlertController mController;

    ActivityMainBinding mBinding;
    private AlertAdapter mAdapter;
    private boolean mSwipeRefreshCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponent().inject(this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.swipe.setOnRefreshListener(this);
        mAdapter = new AlertAdapter(mEventBus, this);
        mBinding.listItem.setAdapter(mAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mEventBus.register(this);
        if (mAdapter != null && mAdapter.getNotifications().isEmpty()) {
            mController.getAlerts(0, LIMIT);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mEventBus.unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNotificationLoaded(AlertEvent event) {
        mBinding.swipe.setRefreshing(false);
        if (event.getSucess()) {
            if (event.getNotifications().isEmpty()) {
                mAdapter.setProgressVisible(false);
            } else {
                if (mSwipeRefreshCall) {
                    mAdapter.clear();
                    mSwipeRefreshCall = false;
                }
                mAdapter.add(mAdapter.getNotifications().size(), event.getNotifications());
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPageRequested(PageRequestEvent event) {
        mController.getAlerts(mAdapter.getNotifications().size(), LIMIT);
    }

    @Override
    public void onRefresh() {
        mBinding.swipe.setRefreshing(true);
        mSwipeRefreshCall = true;
        mController.getAlerts(0, LIMIT);
    }
}
