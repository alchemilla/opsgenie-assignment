package com.example.user.opsgenie_assignment.common.dagger;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.birbit.android.jobqueue.di.DependencyInjector;
import com.example.user.opsgenie_assignment.App;
import com.example.user.opsgenie_assignment.alert.AlertController;
import com.example.user.opsgenie_assignment.common.ApiService;
import com.example.user.opsgenie_assignment.common.L;
import com.example.user.opsgenie_assignment.common.base.BaseJob;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 10/26/2017.
 */

@Module
public class AppModule {
    private final App mApp;

    public AppModule(App app) {
        mApp = app;
    }

    @Provides
    @Singleton
    public EventBus eventBus() {
        return new EventBus();
    }

    @Provides
    @Singleton
    public JobManager jobManager() {
        Configuration config = new Configuration.Builder(mApp)
                .id(Configuration.DEFAULT_ID)
                .customLogger(L.getCustomLogger())
                .consumerKeepAlive(45)
                .maxConsumerCount(3)
                .minConsumerCount(1)
                .injector(new DependencyInjector() {
                    @Override
                    public void inject(Job job) {
                        if (job instanceof BaseJob) {
                            ((BaseJob) job).inject(mApp.getAppComponent());
                        }
                    }
                })
                .build();
        return new JobManager(config);

    }

    @Provides
    @Singleton
    public OkHttpClient okHttpClient() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(loggingInterceptor);
        return builder.build();
    }

    @Provides
    @Singleton
    public ApiService apiService() {
        return new Retrofit.Builder()
                .baseUrl(ApiService.BASE_URL)
                .client(mApp.getAppComponent().okHttpClient())
                .addConverterFactory(mApp.getAppComponent().converterFactory())
                .build()
                .create(ApiService.class);
    }

    @Provides
    @Singleton
    public Gson gson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
    }

    @Provides
    @Singleton
    public Converter.Factory converterFactory() {
        return GsonConverterFactory.create(mApp.getAppComponent().gson());
    }

    @Provides
    @Singleton
    public AlertController notificationController() {
        return new AlertController(mApp.getAppComponent());
    }
}
