package com.example.user.opsgenie_assignment.alert;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by User on 10/26/2017.
 */

public class Alert {
    @SerializedName("id")
    String id;

    @SerializedName("message")
    String message;

    @SerializedName("status")
    String status;

    @SerializedName("owner")
    String owner;

    @SerializedName("createdAt")
    Date createdDate;

    @SerializedName("acknowledged")
    boolean acknowledged;

    @SerializedName("count")
    int count;

    @SerializedName("lastOccurredAt")
    Date lastOccuredDate;

    @SerializedName("source")
    String source;

    @SerializedName("integration")
    Integration integration;

    @SerializedName("description")
    String description;

    @SerializedName("tags")
    List<String> tags;

    @SerializedName("tinyId")
    int tinyId;

    @SerializedName("alias")
    String alias;

    @SerializedName("updatedAt")
    Date updatedAt;

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getOwner() {
        return owner;
    }

    public String getStatus() {
        return status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Integration getIntegration() {
        return integration;
    }

    public boolean getAcknowledged() {
        return acknowledged;
    }

    public String getAckAndStatus() {
        return acknowledged ? "Acked" : "Unacked" + "/" + status.substring(0, 1).toUpperCase() + status.substring(1);
    }

    public String getCountAndLastOccurence() {
        return count + " (Last Occured at " + lastOccuredDate.toString() + ")";
    }

    public String getSource() {
        return source;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getTagsSeparatedByComma() {
        StringBuilder builder = new StringBuilder();
        String mergedTags = "";
        for (int i = 0; i < tags.size(); i++) {
            builder.append(tags.get(i));
            if (i != tags.size() - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    public int getTinyId() {
        return tinyId;
    }

    public String getAlias() {
        return alias;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }
}
