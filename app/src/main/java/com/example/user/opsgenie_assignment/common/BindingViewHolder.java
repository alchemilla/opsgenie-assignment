package com.example.user.opsgenie_assignment.common;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by User on 10/26/2017.
 */

public class BindingViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {

    protected final T mBinding;

    public BindingViewHolder(View view) {
        super(view);
        mBinding = DataBindingUtil.bind(view);
    }

    public BindingViewHolder(T binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public T getBinding() {
        return mBinding;
    }

    public <K extends ViewDataBinding> K getBindingAs(Class<K> clazz) {
        return (K) mBinding;
    }
}
