package com.example.user.opsgenie_assignment.common.dagger;

import com.example.user.opsgenie_assignment.alert.DetailsActivity;
import com.example.user.opsgenie_assignment.alert.AlertListActivity;

import dagger.Component;

/**
 * Created by User on 10/26/2017.
 */

@ActivityScope
@Component(modules = {ActivityModule.class}, dependencies = AppComponent.class)
public interface ActivityComponent extends AppComponent {

    void inject(AlertListActivity activity);

    void inject(DetailsActivity activity);
}

