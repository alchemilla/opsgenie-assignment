package com.example.user.opsgenie_assignment.alert.job;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.example.user.opsgenie_assignment.alert.Alert;
import com.example.user.opsgenie_assignment.alert.event.AlertEvent;
import com.example.user.opsgenie_assignment.common.AlertsReponseList;
import com.example.user.opsgenie_assignment.common.ApiService;
import com.example.user.opsgenie_assignment.common.Constants;
import com.example.user.opsgenie_assignment.common.base.BaseJob;
import com.example.user.opsgenie_assignment.common.dagger.AppComponent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by User on 10/26/2017.
 */

public class GetAlertJob extends BaseJob {

    @Inject
    ApiService mApiService;
    @Inject
    EventBus mEventBus;

    private int mOffset;
    private int mLimit;

    public GetAlertJob(int offset, int limit) {
        super(new Params(PRIORITY_UX).requireNetwork().singleInstanceBy("get-alerts"));
        mLimit = limit;
        mOffset = offset;
    }

    @Override
    public void inject(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        System.out.println("Offset: " + mOffset + " Limit: " + mLimit);
        Response<AlertsReponseList> responseList = mApiService.getAlerts(Constants.API_KEY, mLimit, mOffset).execute();
        if (responseList.body() != null) {
            List<Alert> alertList = responseList.body().getNotifications();
            mEventBus.post(new AlertEvent(true, alertList));
        } else {
            mEventBus.post(new AlertEvent(false, null));
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        System.out.println("Error Message: " + throwable.getMessage());
        mEventBus.post(new AlertEvent(false, null));
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return null;
    }
}
