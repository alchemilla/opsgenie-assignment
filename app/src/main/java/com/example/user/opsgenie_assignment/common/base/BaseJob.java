package com.example.user.opsgenie_assignment.common.base;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.example.user.opsgenie_assignment.common.dagger.AppComponent;

/**
 * Created by User on 10/26/2017.
 */

public abstract class BaseJob extends Job {
    public static final int PRIORITY_MIN = 1;
    public static final int PRIORITY_SYNC = 2;
    public static final int PRIORITY_PREFETCH = 3;
    public static final int PRIORITY_UX = 4;

    protected BaseJob(Params params) {
        super(params);
    }

    public abstract void inject(AppComponent appComponent);

    @Override
    protected int getRetryLimit() {
        return 2;
    }
}
