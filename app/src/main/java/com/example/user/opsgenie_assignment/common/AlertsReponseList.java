package com.example.user.opsgenie_assignment.common;

import android.support.annotation.Nullable;

import com.example.user.opsgenie_assignment.alert.Alert;

import java.util.List;

/**
 * Created by User on 10/26/2017.
 */

public class AlertsReponseList extends ListHolder<Alert> {
    @Nullable
    public List<Alert> getNotifications() {
        List<Alert> alerts = getResultList();
        if (alerts == null) {
            return null;
        }
        return alerts;
    }
}
