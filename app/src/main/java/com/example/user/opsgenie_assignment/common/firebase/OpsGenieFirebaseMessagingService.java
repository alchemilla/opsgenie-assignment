package com.example.user.opsgenie_assignment.common.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import com.example.user.opsgenie_assignment.receiver.AckAlertReceiver;
import com.example.user.opsgenie_assignment.alert.DetailsActivity;
import com.example.user.opsgenie_assignment.R;
import com.example.user.opsgenie_assignment.common.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by User on 10/28/2017.
 */

public class OpsGenieFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private static final String ALERT_ID = "alert_id";
    private static final String ALERT_MESSAGE = "alert_message";

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);

        showNotification(intent);
    }

    private void showNotification(Intent intent) {
        Notification notificationBuilder = null;

        Intent pushIntent = new Intent(getApplicationContext(), DetailsActivity.class);
        pushIntent.putExtra(DetailsActivity.ARG_ALERT_ID, intent.getStringExtra(ALERT_ID));

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, pushIntent,
                PendingIntent.FLAG_ONE_SHOT);

        Intent ackIntent = new Intent(this, AckAlertReceiver.class);
        ackIntent.putExtra(AckAlertReceiver.ALERT_ID, intent.getStringExtra(ALERT_ID));
        PendingIntent pendingIntentAck = PendingIntent.getBroadcast(this, 0, ackIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        try {
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)          // don't need to pass icon with your message if it's already in your app !
                    .setContentTitle(URLDecoder.decode(getString(R.string.app_name), "UTF-8"))
                    .setContentText(URLDecoder.decode(intent.getStringExtra(ALERT_MESSAGE), "UTF-8"))
                    .addAction(R.drawable.ic_spellcheck_black, "ACK", pendingIntentAck) // #0
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(pendingIntent)
                    .build();

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(Constants.NOTIFICATION_ID_ALERT, notificationBuilder);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
