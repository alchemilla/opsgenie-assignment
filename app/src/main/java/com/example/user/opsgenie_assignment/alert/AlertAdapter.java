package com.example.user.opsgenie_assignment.alert;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.opsgenie_assignment.R;
import com.example.user.opsgenie_assignment.alert.event.PageRequestEvent;
import com.example.user.opsgenie_assignment.common.BindingViewHolder;
import com.example.user.opsgenie_assignment.databinding.NotificationItemBinding;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 10/26/2017.
 */

public class AlertAdapter extends RecyclerView.Adapter<BindingViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_PROGRESS = 1;

    private static final int PREFETCH_THRESHOLD = 11;
    private EventBus mEventBus;
    private Context mContext;
    private List<Alert> mAlerts;
    private boolean mPageRequested;
    private boolean mProgressVisible;

    public AlertAdapter(EventBus eventBus, Context context) {
        mAlerts = new ArrayList<>();
        mEventBus = eventBus;
        mProgressVisible = true;
        mContext = context;
    }

    public void clear() {
        mAlerts.clear();
    }

    public void add(int position, List<Alert> alerts) {
        boolean oldEmpty = mAlerts.isEmpty();
        mAlerts.addAll(position, alerts);
        if (!oldEmpty) {
            notifyItemRangeInserted(position, alerts.size());
        } else {
            notifyDataSetChanged();
        }
        mPageRequested = false;
    }

    public List<Alert> getNotifications() {
        return mAlerts;
    }

    @Override
    public BindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
            return new ProgressBindingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(BindingViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder bindingHolder = (ViewHolder) holder;
            bindingHolder.getBinding().setAlert(mAlerts.get(position));
            bindingHolder.getBinding().executePendingBindings();

            if (!mPageRequested && position >= mAlerts.size() - PREFETCH_THRESHOLD) {
                mPageRequested = true;
                mEventBus.post(new PageRequestEvent());
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mProgressVisible && position == mAlerts.size()) {
            return TYPE_PROGRESS;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        if (mProgressVisible) {
            return mAlerts.size() + 1;
        } else {
            return mAlerts.size();
        }
    }

    public synchronized void setProgressVisible(boolean progressVisible) {
        boolean oldVisible = mProgressVisible;
        mProgressVisible = progressVisible;
        if (!mAlerts.isEmpty()) {
            if (oldVisible && !progressVisible) {
                notifyItemRemoved(mAlerts.size() + 1);
            } else if (!oldVisible && progressVisible) {
                notifyItemInserted(mAlerts.size());
            }
        }
    }

    public class ViewHolder extends BindingViewHolder<NotificationItemBinding> implements View.OnClickListener {

        public ViewHolder(View view) {
            super(view);
            getBinding().getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == getBinding().getRoot()) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra(DetailsActivity.ARG_ALERT_ID, getBinding().getAlert().getId());
                mContext.startActivity(intent);
            }
        }
    }

    private static class ProgressBindingViewHolder extends BindingViewHolder {

        public ProgressBindingViewHolder(View view) {
            super(view);
        }
    }
}
