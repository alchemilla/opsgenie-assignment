package com.example.user.opsgenie_assignment.receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.user.opsgenie_assignment.App;
import com.example.user.opsgenie_assignment.alert.AlertController;
import com.example.user.opsgenie_assignment.common.Constants;

import javax.inject.Inject;

/**
 * Created by User on 10/28/2017.
 */

public class AckAlertReceiver extends BroadcastReceiver {
    public static final String ALERT_ID = "alert_id";

    @Inject
    AlertController mAlertController;

    @Override
    public void onReceive(Context context, Intent intent) {
        ((App) context.getApplicationContext()).getAppComponent().inject(this);

        String alertId = intent.getStringExtra(ALERT_ID);
        mAlertController.ackAlert(alertId);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Constants.NOTIFICATION_ID_ALERT);
    }
}
