package com.example.user.opsgenie_assignment.common;

import com.example.user.opsgenie_assignment.alert.Alert;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by User on 10/26/2017.
 */

public interface ApiService {
    String BASE_URL = "https://api.opsgenie.com/v2/";

    @GET("alerts")
    Call<AlertsReponseList> getAlerts(@Query("apiKey") String apiKey, @Query("limit") int limit, @Query("offset") int offset);

    @GET
    Call<ApiResponse<Alert>> getAlertDetail(@Url String url, @Query("apiKey") String apiKey, @Query("identifierType") String type);

    @POST("alerts/{id}/acknowledge")
    Call<AckResponse> ackAlert(@Path("id") String id, @Query("apiKey") String apiKey, @Query("identifierType") String type, @Body JSONObject body);
}
