package com.example.user.opsgenie_assignment.common.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.user.opsgenie_assignment.App;
import com.example.user.opsgenie_assignment.common.dagger.ActivityComponent;
import com.example.user.opsgenie_assignment.common.dagger.ActivityModule;
import com.example.user.opsgenie_assignment.common.dagger.DaggerActivityComponent;

/**
 * Created by User on 10/26/2017.
 */

public class BaseActivity extends AppCompatActivity {

    private ActivityComponent mComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mComponent = DaggerActivityComponent.builder()
                .appComponent(getApp().getAppComponent())
                .activityModule(new ActivityModule(this))
                .build();
    }

    public ActivityComponent getComponent() {
        return mComponent;
    }

    protected App getApp() {
        return (App) getApplicationContext();
    }
}
