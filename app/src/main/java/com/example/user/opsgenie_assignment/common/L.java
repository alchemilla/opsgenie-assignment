package com.example.user.opsgenie_assignment.common;

/**
 * Created by User on 10/27/2017.
 */


import android.util.Log;

import com.birbit.android.jobqueue.log.CustomLogger;

public class L {

    private static final String TAG = "OpsGenie";
    private static final CustomLogger CUSTOM_LOGGER = new CustomLogger() {
        @Override
        public boolean isDebugEnabled() {
            return true;
        }

        @Override
        public void d(String text, Object... args) {
        }

        @Override
        public void e(Throwable t, String text, Object... args) {
            L.e(t, text, args);
        }

        @Override
        public void e(String text, Object... args) {
            L.e(text, args);
        }

        @Override
        public void v(String s, Object... objects) {
        }
    };

    public static void v(String msg, Object... args) {
        Log.v(TAG, String.format(msg, args));
    }

    public static void d(String msg, Object... args) {
        Log.d(TAG, String.format(msg, args));
    }

    public static void i(String msg, Object... args) {
        String message = String.format(msg, args);
        Log.i(TAG, message);
    }

    public static void w(String msg, Object... args) {
        String message = String.format(msg, args);
        Log.w(TAG, message);
    }

    public static void w(Throwable t, String msg, Object... args) {
        w(msg, args);
    }

    public static void e(String msg, Object... args) {
        String message = String.format(msg, args);
        Log.e(TAG, message);
    }

    public static void e(Throwable t, String msg, Object... args) {
        e(msg, args);
    }

    public static void setProcessName(String processName) {
    }

    public static CustomLogger getCustomLogger() {
        return CUSTOM_LOGGER;
    }
}
