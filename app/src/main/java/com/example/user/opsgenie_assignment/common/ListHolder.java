package com.example.user.opsgenie_assignment.common;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 10/26/2017.
 */

public class ListHolder<T> {

    @SerializedName("data")
    private List<T> mResultList;

    public List<T> getResultList() {
        return mResultList;
    }
}