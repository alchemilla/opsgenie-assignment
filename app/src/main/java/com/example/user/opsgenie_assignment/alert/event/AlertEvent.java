package com.example.user.opsgenie_assignment.alert.event;

import com.example.user.opsgenie_assignment.alert.Alert;

import java.util.List;

/**
 * Created by User on 10/26/2017.
 */

public class AlertEvent {
    private List<Alert> mAlerts;
    private boolean mSuccess;

    public AlertEvent(boolean success, List<Alert> alerts) {
        mSuccess = success;
        mAlerts = alerts;
    }

    public List<Alert> getNotifications() {
        return mAlerts;
    }

    public boolean getSucess() {
        return mSuccess;
    }
}
