package com.example.user.opsgenie_assignment.common;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 10/28/2017.
 */

public class AckResponse {

    @SerializedName("result")
    String result;

    public String getResult() {
        return result;
    }
}
