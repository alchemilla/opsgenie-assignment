package com.example.user.opsgenie_assignment.common.dagger;

import android.app.Activity;

import dagger.Module;

/**
 * Created by User on 10/26/2017.
 */

@Module
public class ActivityModule {

    private final Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }
}
