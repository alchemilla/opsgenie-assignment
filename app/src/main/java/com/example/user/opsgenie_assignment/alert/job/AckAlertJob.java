package com.example.user.opsgenie_assignment.alert.job;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.example.user.opsgenie_assignment.alert.event.AlertAckedEvent;
import com.example.user.opsgenie_assignment.common.AckResponse;
import com.example.user.opsgenie_assignment.common.ApiService;
import com.example.user.opsgenie_assignment.common.Constants;
import com.example.user.opsgenie_assignment.common.base.BaseJob;
import com.example.user.opsgenie_assignment.common.dagger.AppComponent;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by User on 10/28/2017.
 */

public class AckAlertJob extends BaseJob {

    @Inject
    ApiService mApiService;
    @Inject
    EventBus mEventBus;

    private String mAlertId;

    public AckAlertJob(String alertid) {
        super(new Params(PRIORITY_UX).requireNetwork().singleInstanceBy("ack-alert"));
        mAlertId = alertid;
    }

    @Override
    public void inject(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("user", "Monitoring Script");
        jsonObject.put("source", "AWS Lambda");
        jsonObject.put("note", "Action executed via Alert API");

        Response<AckResponse> response = mApiService.ackAlert(mAlertId, Constants.API_KEY, "id", jsonObject).execute();
        if (response.body() != null) {
            mEventBus.post(new AlertAckedEvent(true));
        } else {
            mEventBus.post(new AlertAckedEvent(false));
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        mEventBus.post(new AlertAckedEvent(false));
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return null;
    }
}
