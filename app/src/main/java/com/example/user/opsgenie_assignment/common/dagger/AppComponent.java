package com.example.user.opsgenie_assignment.common.dagger;

import com.birbit.android.jobqueue.JobManager;
import com.example.user.opsgenie_assignment.receiver.AckAlertReceiver;
import com.example.user.opsgenie_assignment.alert.job.AckAlertJob;
import com.example.user.opsgenie_assignment.alert.AlertController;
import com.example.user.opsgenie_assignment.common.ApiService;
import com.example.user.opsgenie_assignment.alert.job.GetAlertDetailJob;
import com.example.user.opsgenie_assignment.alert.job.GetAlertJob;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Converter;

/**
 * Created by User on 10/26/2017.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    JobManager jobManager();

    EventBus eventBus();

    ApiService apiService();

    Converter.Factory converterFactory();

    Gson gson();

    AlertController notificationController();

    OkHttpClient okHttpClient();

    void inject(GetAlertJob job);

    void inject(AlertController controller);

    void inject(GetAlertDetailJob job);

    void inject(AckAlertJob job);

    void inject(AckAlertReceiver receiver);

}
