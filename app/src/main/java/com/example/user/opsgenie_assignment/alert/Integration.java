package com.example.user.opsgenie_assignment.alert;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 10/29/2017.
 */

public class Integration {
    @SerializedName("id")
    String id;

    @SerializedName("name")
    String name;

    @SerializedName("type")
    String type;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}
