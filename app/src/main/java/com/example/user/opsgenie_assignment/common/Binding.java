package com.example.user.opsgenie_assignment.common;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.text.format.DateUtils;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by User on 10/28/2017.
 */

public class Binding {

    public static List<Long> times = Arrays.asList(
            TimeUnit.DAYS.toMillis(365),
            TimeUnit.DAYS.toMillis(30),
            TimeUnit.DAYS.toMillis(1),
            TimeUnit.HOURS.toMillis(1),
            TimeUnit.MINUTES.toMillis(1),
            TimeUnit.SECONDS.toMillis(1));

    public static List<String> timesString = Arrays.asList("year", "month", "day", "hour", "minute", "second");


    @BindingAdapter(value = {"relativeTime", "resolutionMs", "precisionDay"}, requireAll = false)
    public static void formatRelativeTime(TextView view, Date date, long resolutionMs,
                                          int precisionDay) {
        if (date != null) {
            if (precisionDay > 0 && new DateTime(date).plusDays(precisionDay).isAfterNow()) {
                formatShortTime(view, date);
                return;
            }
            if (resolutionMs == 0L) {
                resolutionMs = DateUtils.DAY_IN_MILLIS;
            }
            view.setText(DateUtils.getRelativeTimeSpanString(date.getTime(),
                    System.currentTimeMillis(), resolutionMs, DateUtils.FORMAT_ABBREV_ALL));
        }
    }

    @BindingAdapter(value = {"android:drawableLeft"})
    public static void setDrawableLeft(TextView view, Drawable drawable) {
        view.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
    }

    @BindingAdapter("shortTime")
    public static void formatShortTime(TextView view, Date date) {
        if (date != null) {
            view.setText(DateTimeFormat.shortTime().print(new DateTime(date)));
        }
    }

    @BindingAdapter(value = {"toDuration"})
    public static void toDuration(TextView view, long duration) {

        StringBuffer res = new StringBuffer();
        for (int i = 0; i < times.size(); i++) {
            Long current = times.get(i);
            long temp = duration / current;
            if (temp > 0) {
                res.append(temp).append(" ").append(timesString.get(i)).append(temp != 1 ? "s" : "").append(" ago");
                break;
            }
        }
        if ("".equals(res.toString()))
            view.setText("0s");
        else {
            String builded = res.toString();
            String result = builded.substring(0, builded.indexOf(" ")) + builded.substring(builded.indexOf(" ") + 1, builded.indexOf(" ") + 2);
            view.setText(result);
        }

    }
}
