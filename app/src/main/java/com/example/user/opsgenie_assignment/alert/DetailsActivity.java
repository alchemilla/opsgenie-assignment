package com.example.user.opsgenie_assignment.alert;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.user.opsgenie_assignment.R;
import com.example.user.opsgenie_assignment.alert.event.AlertAckedEvent;
import com.example.user.opsgenie_assignment.alert.event.AlertDetailLoadedEvent;
import com.example.user.opsgenie_assignment.common.Constants;
import com.example.user.opsgenie_assignment.common.base.BaseActivity;
import com.example.user.opsgenie_assignment.databinding.ActivityDetailBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

/**
 * Created by User on 10/27/2017.
 */

public class DetailsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static final String ARG_ALERT_ID = "alert_id";

    @Inject
    AlertController mController;
    @Inject
    EventBus mEventBus;

    ActivityDetailBinding mBinding;
    private String mAlertId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        setSupportActionBar(mBinding.toolbar);
        mBinding.swipe.setOnRefreshListener(this);
        mAlertId = getIntent().getStringExtra(ARG_ALERT_ID);
        mController.getAlertDetail(mAlertId, Constants.API_KEY);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mEventBus.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mEventBus.unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.acknowledge:
                mController.ackAlert(mAlertId);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlertDetailLoaded(AlertDetailLoadedEvent event) {
        mBinding.swipe.setRefreshing(false);
        if (event.getSuccess()) {
            mBinding.setAlert(event.getAlert());
            mBinding.executePendingBindings();
        } else {
            Toast.makeText(this, "Error Occured", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlertAcked(AlertAckedEvent event) {
        if (event.getSuccess()) {
            Alert ackedAlert = mBinding.getAlert();
            ackedAlert.acknowledged = true;
            mBinding.setAlert(ackedAlert);
            mBinding.executePendingBindings();
        }
    }

    @Override
    public void onRefresh() {
        mBinding.swipe.setRefreshing(true);
        mController.getAlertDetail(mAlertId, Constants.API_KEY);
    }
}
