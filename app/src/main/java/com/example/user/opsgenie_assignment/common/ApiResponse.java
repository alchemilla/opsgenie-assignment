package com.example.user.opsgenie_assignment.common;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 10/26/2017.
 */

public class ApiResponse<T> {
    @SerializedName("data")
    private T data;

    public T getData() {
        return data;
    }
}
