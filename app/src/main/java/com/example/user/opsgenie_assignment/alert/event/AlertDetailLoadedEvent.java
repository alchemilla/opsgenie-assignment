package com.example.user.opsgenie_assignment.alert.event;

import com.example.user.opsgenie_assignment.alert.Alert;

/**
 * Created by User on 10/27/2017.
 */

public class AlertDetailLoadedEvent {

    private Alert mAlert;
    private boolean mSuccess;

    public AlertDetailLoadedEvent(Alert alert, boolean success) {
        mAlert = alert;
        mSuccess = success;
    }

    public Alert getAlert() {
        return mAlert;
    }

    public boolean getSuccess() {
        return mSuccess;
    }
}
