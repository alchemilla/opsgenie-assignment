package com.example.user.opsgenie_assignment.alert.event;

/**
 * Created by User on 10/29/2017.
 */

public class AlertAckedEvent {

    private boolean mSuccess;

    public AlertAckedEvent(boolean success) {
        mSuccess = success;
    }

    public boolean getSuccess() {
        return mSuccess;
    }
}
